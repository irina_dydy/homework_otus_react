import React from 'react';
import './CreateUser.css';

class CreateUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            username: '',
            password: '',
            email: '',
            phone: ''
        };
    }

    onNameChanged = v => {
        this.setState({
            name: v.target.value,
            username: this.state.username,
            password: this.state.password,
            email: this.state.email,
            phone: this.state.phone
        });
    };

    onUsernameChanged = v => {
        this.setState({
            name: this.state.name,
            username: v.target.value,
            password: this.state.password,
            email: this.state.email,
            phone: this.state.phone
        });
    };
    onPasswordChanged = v => {
        this.setState({
            name: this.state.name,
            username: this.state.username,
            password: v.target.value,
            email: this.state.email,
            phone: this.state.phone
        });
    };
    onEmailChanged = v => {
        this.setState({
            name: this.state.name,
            username: this.state.username,
            password: this.state.password,
            email: v.target.value,
            phone: this.state.phone
        });
    };
    onPhoneChanged = v => {
        this.setState({
            name: this.state.name,
            username: this.state.username,
            password: this.state.password,
            email: this.state.email,
            phone: v.target.value
        });
    };

    createUser = () => {
        fetch('/users', {
            method: 'POST',
            body: JSON.stringify({
                name: this.name,
                username: this.username,
                password: this.password,
                email: this.email,
                phone: this.phone
            })
        })
        .then(r => r.json())
        .then(j => alert('Created user with id = ' + j.id));
    }

    render() {
        return (
            <form>
                <p>
                    <label>Name: </label>
                    <input
                        type="text"
                        value={this.state.name}
                        onChange={this.onNameChanged} />
                </p>
                <p>
                    <label>Username: </label>
                    <input name="username"
                        type="text"
                        value={this.state.username}
                        onChange={this.onUsernameChanged} />
                </p>
                <p>
                    <label>Password: </label>
                    <input name="password"
                        type="password"
                        value={this.state.password}
                        onChange={this.onPasswordChanged} />
                </p>
                <p>
                    <label>Email: </label>
                    <input name="email"
                        type="email"
                        value={this.state.email}
                        onChange={this.onEmailChanged} />
                </p>
                <p>
                    <label>Phone: </label>
                    <input name="phone"
                        type="phone"
                        value={this.state.phone}
                        onChange={this.onPhoneChanged} />
                </p>
                <button type="button" onClick={this.createUser}>Register</button>
            </form>
        );
    }
}

export default CreateUser;